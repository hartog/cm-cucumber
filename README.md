# CM Cucumber

A very simple POC for java8 Cucumber to run against the CM SOAP.

## Where to look

The Cucumber features and scenario are found in

`src/test/resources/com/hrs/cm/cucumber/*.feature`

The following .java files perform these tasks:

- `src/test/java/com/hrs/cm/cucumber/AbstractSecureWsHanlder.java`
  & `src/test/java/com/hrs/cm/cucumber/TestClient.java`
  implement a WSDL-less testing client for the CM SOAP Service
- `src/test/java/com/hrs/cm/cucumber/RunCukesTest.java`
  just a stub to enable cucumber-junit
- `src/test/java/com/hrs/cm/cucumber/ConnectStepdefs.java`
  implements steps needed to execute the Gherkin found in the feature files

The output of the tests is stored target/cucumber/index.html for inspection in the browser

## How to run?

´mvn test´
