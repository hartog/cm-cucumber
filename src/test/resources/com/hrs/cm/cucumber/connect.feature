Feature: Basic connectivity to the SOAP interface
    This is a basic test in cucumber for the ChannelManager interface
    It should fetch the wsdl
    It should perform a ping

    Scenario: Fetching the WSDL from DET1
        Given a wsdl located at "http://det1-hsv3ws-frontend-111.hrs.de:8080/hsv3/014/HSVService?wsdl"
        When this wsdl is fetched
        Then I have its contents
        And they should be larger then 1024 bytes

    Scenario: Ping
        Given a soap server listening on "https://det1-hsv3ws-frontend-111.hrs.de:8443/hsv3/014/HSVService"
        When I execute a ping request with "echo" as data
        Then I should receive "echo" back
