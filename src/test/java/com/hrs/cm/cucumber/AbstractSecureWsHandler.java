package com.hrs.cm.cucumber;

import com.hrs.ws.hsv.v016.generated.*;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public abstract class AbstractSecureWsHandler {

    /**
     * Enable Services for HTTPS Requests
     * @param serviceBean
     */
    public static void configureSecureTransaction(HSVSoapService serviceBean) {
        Client client = ClientProxy.getClient(serviceBean);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        String targetAddress = http.getTarget().getAddress().getValue();

        if (targetAddress.toLowerCase().startsWith("https:")) {
            TrustManager[] simpleTrustManager = new TrustManager[] { new X509TrustManager() {
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            } };
            TLSClientParameters tlsParams = new TLSClientParameters();
            tlsParams.setTrustManagers(simpleTrustManager);
            tlsParams.setDisableCNCheck(true);
            http.setTlsClientParameters(tlsParams);
        }
    }
}