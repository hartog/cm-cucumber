package com.hrs.cm.cucumber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Scanner;

import cucumber.api.java8.En;

public class ConnectStepdefs implements En {
    String wsdl;
    String contents;
    TestClient client;

    public ConnectStepdefs() {
        this.wsdlScenario();
        this.pingScenario();
    }

    private void wsdlScenario() {
        Given("^a wsdl located at \"([^\"]*)\"$", (String arg1) -> {
            this.wsdl = arg1;
        });
        
        When("^this wsdl is fetched$", () -> {
            String out = new Scanner(new URL(this.wsdl).openStream(), "UTF-8").useDelimiter("\\A").next();
            this.contents = out;
        });
        
        Then("^I have its contents$", () -> {
            assertNotNull("the WSDL is missing", this.contents);
        });

        Then("^they should be larger then (\\d+) bytes$", (Integer arg1) -> {
            String msg = String.format("WSDL seems to be less then %d bytes: %d",arg1,this.contents.length());
			assertTrue(msg, this.contents.length() > arg1);
        });
    }

    private void pingScenario() {
        Given("^a soap server listening on \"([^\"]*)\"$", (String url) -> {
            this.client = new TestClient(url);
        });
        
        When("^I execute a ping request with \"([^\"]*)\" as data$", (String data) -> {
            this.contents = this.client.ping(data);
        });
        
        Then("^I should receive \"([^\"]*)\" back$", (String data) -> {
            assertEquals("That does not go well", data, this.contents);
        });  
    }
}