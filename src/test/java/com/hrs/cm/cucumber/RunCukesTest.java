package com.hrs.cm.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(plugin = {"pretty", "html:target/cucumber"})
public class RunCukesTest {
    // this is an empty stub to get you cucumba going
}