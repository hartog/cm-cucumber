package com.hrs.cm.cucumber;

import com.hrs.ws.hsv.v016.generated.*;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.ClientImpl.EchoContext;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class TestClient extends AbstractSecureWsHandler {

    private String serviceUrl = "http://127.0.0.1:65024/exprws";
    private JaxWsProxyFactoryBean factory;
    private HSVSoapService soapService;
    private Client client;
    
    public TestClient(String serviceUrl) {
        this.serviceUrl = serviceUrl;

        factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(HSVSoapService.class);
        factory.setAddress(this.serviceUrl);

        soapService = (HSVSoapService) factory.create();

        client = ClientProxy.getClient(soapService);
        client.getInInterceptors().add(new LoggingInInterceptor());
        client.getOutInterceptors().add(new LoggingOutInterceptor());

        configureSecureTransaction(soapService);
    }
    
    public String ping(String echo) throws Exception {
        // Request for Order Details
        HRSPingRequest request = new HRSPingRequest();
        request.setEchoData(echo);
        HRSPingResponse response = soapService.ping(request);
        return response.getEchoData();
    }
}